from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/test")
def test():
    return "<h2>Hello, this test route also works!</h2>"

@app.route("/test2")
def test2():
    return "<h2>Hello, this test2 route also works!</h2>"

@app.route("/new")
def new_route():
    return "<h2>this new route also works as intended</h2>"


if __name__ == "__main__":
    app.run()