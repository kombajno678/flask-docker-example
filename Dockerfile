FROM python:buster

WORKDIR /usr/src/app

ENV FLASK_APP=main.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_ENV=development

COPY ./flask/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY ./flask .
CMD ["flask", "run"]