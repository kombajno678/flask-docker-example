terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.4.1"
    }
  }
}
provider "kubernetes" {
  # override ip, bc using minikube and kubectl proxy
  host = var.kubernetesApi
}
resource "kubernetes_namespace" "website" {
  metadata {
    name = var.namespace
  }
}


resource "kubernetes_deployment" "website" {
  metadata {
    name      = var.deployment
    namespace = kubernetes_namespace.website.metadata.0.name
  }
  spec {
    replicas = var.replicas
    selector {
      match_labels = {
        app = var.appLabel
      }
    }

    template {
      metadata {
        labels = {
          app = var.appLabel
        }
      }

      spec {
        container {
          image = var.image
          name  = "${var.appLabel}-container"
          port {
            container_port = var.ports.internal
          }
        #   volume_mount {
        #     read_only  = true
        #     mount_path = "/run/auth"
        #     name       = "something"
        #   }
        }
        # volume {
        #   name = "something"
        #   secret {
        #     secret_name = kubernetes_secret.secret.metadata.0.name
        #   }
        # }
      }

    }
  }
}
resource "kubernetes_service" "website" {
  metadata {
    name      = var.service
    namespace = kubernetes_namespace.website.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.website.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      node_port   = 30201
      port        = var.ports.internal
      target_port = var.ports.internal
    }
  }
}


