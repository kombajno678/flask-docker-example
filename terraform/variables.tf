variable "namespace" {
  type    = string
  default = "jenkins-ci-flask"
}
variable "service" {
  type    = string
  default = "website"
}
variable "deployment" {
  type    = string
  default = "website"
}
variable "appLabel" {
  type    = string
  default = "website"
}
variable "image" {
  type    = string
  default = "mys7erion/flask-docker-example"
}
variable "replicas" {
  type    = number
  default = 1
}
variable "ports" {
  type = object({
    internal = number
  })
  default = {
    internal = 80
  }
}
variable "kubernetesApi" {
    type = string
    default = "http://127.0.0.1:8001" 
}
